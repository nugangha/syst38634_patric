package ca.sheridancollege.trejo;

/**
 * @author dancye
 * @author ramses
 *
 */

public class Person{

	
	
	private String name;
	private String role;
	public Person(String givenRole, String givenName) 
	{
		name = givenName;
		role = givenRole;
	}
	
	public String toString()
	{
		return "team: " + this.getClass().getSimpleName( ) + " - " +  role + ": " + name ;
		
	}
}
